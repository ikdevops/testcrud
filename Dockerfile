FROM openjdk:17-jdk-alpine
VOLUME /tmp
ADD target/crud-0.0.1-SNAPSHOT.jar crud-app.jar

ENTRYPOINT ["java", "-jar", "crud-app.jar"]
EXPOSE 2222
