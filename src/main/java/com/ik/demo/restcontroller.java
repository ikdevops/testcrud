package com.ik.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class restcontroller {
	
	
	@GetMapping("/public")
	public String getPublicResource(HttpServletRequest httpServletRequest) {
		return " this is a public ";
	}

	@GetMapping("/private")
	public String getPrivateResource(){
		return " this is a private ";
	}
}
